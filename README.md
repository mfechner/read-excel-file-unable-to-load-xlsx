# How to reproduce
Clone the repository.

Tested with node (v16.10.0) and npm (v7.23.0).

Reproduce it:
```
npm i
npm run start
```

Error:
```
> node app.js

H:\read-excel-file-unable-to-load-xlsx\node_modules\jszip\lib\zipEntries.js:168
                throw new Error("Can't find end of central directory : is this a zip file ? " +
                      ^

Error: Can't find end of central directory : is this a zip file ? If it is, see https://stuk.github.io/jszip/documentation/howto/read_zip.html
     at ZipEntries.readEndOfCentral (H:\read-excel-file-unable-to-load-xlsx\node_modules\jszip\lib\zipEntries.js:168:23)
    at ZipEntries.load (H:\read-excel-file-unable-to-load-xlsx\node_modules\jszip\lib\zipEntries.js:256:14)
    at H:\read-excel-file-unable-to-load-xlsx\node_modules\jszip\lib\load.js:48:24

```
